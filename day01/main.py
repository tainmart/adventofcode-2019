from math import floor
data = [i for i in open('input').readlines()]


def calculateFuel(amount):
    return floor(amount / 3) - 2


if __name__ == "__main__":
    sum = 0
    for line in data:
        sum += calculateFuel(int(line))
    print("Solution part 1: {}".format(sum))

    print("-" * 25)

    sum = 0
    for line in data:
        fuel = int(line)
        while fuel > 8:
            fuel = calculateFuel(fuel)
            sum += fuel
    print("Solution part 2: {}".format(sum))
