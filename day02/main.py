from itertools import product
data = [int(i) for i in open('input').read().split(',')]


def parseIntCodes(program, instruction_pointer):
    opcode = program[instruction_pointer]
    parameter1 = program[instruction_pointer + 1]
    parameter2 = program[instruction_pointer + 2]
    parameter3 = program[instruction_pointer + 3]

    return (opcode, parameter1, parameter2, parameter3)


if __name__ == "__main__":
    instruction_pointer = 0
    program = data.copy()
    program[1] = 12
    program[2] = 2

    while True:
        (opcode, input1, input2, destination) = parseIntCodes(
            program, instruction_pointer)
        if opcode == 1:
            program[destination] = program[input1] + program[input2]
        elif opcode == 2:
            program[destination] = program[input1] * program[input2]
        elif opcode == 99:
            break
        else:
            raise ValueError('Unexepected opcode')
        instruction_pointer += 4

    print("Solution part 1: {}".format(program[0]))

    print("-" * 25)

    for (noun, verb) in product(range(0, 100), repeat=2):
        instruction_pointer = 0
        program = data.copy()
        program[1] = noun
        program[2] = verb

        while True:
            (opcode, input1, input2, destination) = parseIntCodes(
                program, instruction_pointer)
            if opcode == 1:
                program[destination] = program[input1] + program[input2]
            elif opcode == 2:
                program[destination] = program[input1] * program[input2]
            elif opcode == 99:
                break
            else:
                raise ValueError('Unexepected opcode')
            instruction_pointer += 4

        if program[0] == 19690720:
            print("Solution part 2: {}".format(100 * noun + verb))
            break
