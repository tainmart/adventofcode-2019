data = [i for i in open('input').readlines()]


def drawWire(wire_data):
    x = 0
    y = 0
    length = 0
    grid = {}
    for instruction in wire_data.split(','):
        direction = instruction[0]
        for _ in range(int(instruction[1:])):
            if direction == 'U':
                x += 1
            elif direction == 'D':
                x -= 1
            elif direction == 'R':
                y += 1
            elif direction == 'L':
                y -= 1
            length += 1
            if (x, y) not in grid:
                grid[(x, y)] = length

    return grid


if __name__ == "__main__":
    grid = []
    for index, wire in enumerate(data):
        grid.append(drawWire(wire))

    intersections = set(grid[0].keys()) & set(grid[1].keys())
    closest = min([abs(x) + abs(y) for (x, y) in intersections])
    print("Solution part 1: {}".format(closest))

    print("-" * 25)

    closestLength = min([grid[0][intersection] + grid[1][intersection]
                         for intersection in intersections])
    print("Solution part 2: {}".format(closestLength))
