data = range(353096, 843212)

if __name__ == "__main__":
    amount = 0
    for password in data:
        password = str(password)
        if len(set(password)) is len(list(password)):
            continue
        if list(password) != sorted(password):
            continue
        amount += 1
    print("Solution part 1: {}".format(amount))

    print("-" * 25)

    amount = 0
    for password in data:
        password = str(password)
        if 2 not in map(password.count, password):
            continue
        if list(password) != sorted(password):
            continue
        amount += 1
    print("Solution part 2: {}".format(amount))
