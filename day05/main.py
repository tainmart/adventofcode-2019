data = [int(i) for i in open('input').read().split(',')]


def step(program, instruction_pointer, input):
    instruction = str(program[instruction_pointer]).zfill(5)
    opcode = int(instruction[-2:])

    if opcode == 99:
        return None

    parameter1 = program[instruction_pointer + 1]
    parameter2 = program[instruction_pointer + 2]
    parameter3 = None
    mode1 = int(instruction[2])
    mode2 = int(instruction[1])

    value1 = parameter1
    value2 = parameter2
    if opcode not in [3, 4, 99]:
        if mode1 == 0:
            value1 = program[parameter1]
        if mode2 == 0:
            value2 = program[parameter2]
        parameter3 = program[instruction_pointer + 3]

    if opcode == 1:
        instruction_pointer += 4
        program[parameter3] = value1 + value2
    elif opcode == 2:
        instruction_pointer += 4
        program[parameter3] = value1 * value2
    elif opcode == 3:
        instruction_pointer += 2
        program[parameter1] = input
    elif opcode == 4:
        instruction_pointer += 2
        if mode1 == 0:
            value1 = program[parameter1]
        print(value1)
    elif opcode == 5:
        instruction_pointer += 3
        if value1 != 0:
            instruction_pointer = value2
    elif opcode == 6:
        instruction_pointer += 3
        if value1 == 0:
            instruction_pointer = value2
    elif opcode == 7:
        instruction_pointer += 4
        program[parameter3] = int(value1 < value2)
    elif opcode == 8:
        instruction_pointer += 4
        program[parameter3] = int(value1 == value2)
    else:
        raise ValueError('Unexepected opcode')
    return instruction_pointer


if __name__ == "__main__":
    instruction_pointer = 0
    program = data.copy()
    while instruction_pointer is not None:
        instruction_pointer = step(program, instruction_pointer, 1)

    print("-" * 25)

    instruction_pointer = 0
    program = data.copy()
    while instruction_pointer is not None:
        instruction_pointer = step(program, instruction_pointer, 5)
