import networkx as nx
data = [tuple(line.strip().split(')')) for line in open('input').readlines()]

if __name__ == "__main__":
    graph = nx.DiGraph()
    graph.add_edges_from(data)
    size = nx.transitive_closure(graph).size()
    print("Solution part 1: {}".format(size))
    print("-" * 25)
    shortestPath = nx.shortest_path_length(
        graph.to_undirected(), 'YOU', 'SAN') - 2
    print("Solution part 2: {}".format(shortestPath))
